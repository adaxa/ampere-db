FROM postgres:15.0-bullseye
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
    && apt-get update && apt-get install -y locales locales-all \
    && rm -rf /var/lib/apt/lists/*
ENV POSTGRES_USER ampere
ENV POSTGRES_PASSWORD ampere
COPY /create-databases.sh /docker-entrypoint-initdb.d/
COPY /init-db.sql /docker-entrypoint-initdb.d/
